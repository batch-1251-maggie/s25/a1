
//find users with letter 'a' in their first or last name.

db.users.find(
	{
		$or:[
			{"firstName":{$regex: "A", $options: "i"}},
			{"lastName":{$regex: "A", $options: "i"}}
		]
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1
	}
);

//find users who are admins and is active.

db.users.find(
	{
		$and:[
			{"isAdmin": true},
			{"isActive": true}
		]
	}
);

//find courses with letter 'u' in its name and has a price of greater
// than or equal to 13000.

db.courses.find(
	{
		$and:[
			{"name":{$regex: "U", $options: "i"}},
			{"price": {$gte: 13000.00}}
		]
	}	
);